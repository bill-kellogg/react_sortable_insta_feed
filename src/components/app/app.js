import React, { Component } from 'react';
import styles from './app.css';
import SortByLikes from '../buttons/sort_by_likes.js';
import SortByDate from '../buttons/sort_by_date.js';

class App extends Component {

	constructor(props) {
		super();

		this.state = {
			jsonData: null,
			sortedByDate: true,
			sortedByLikes: false
		}

		this.handleSortByDate = this.handleSortByDate.bind(this);
		this.handleSortByLikes = this.handleSortByLikes.bind(this);
	}

	fetchInstagramFeed() {
		fetch('https://api.massrelevance.com/4qcxbv4tpw/vspink_instagram.json?page_links=1&limit=6').
			then(response => response.json()).
			then(data => {
				
				let filteredData = data.map( (elem) => {
					return { 
						id: elem.id,
						timestamp: elem.created_time,
						image_url: elem.images.standard_resolution.url,
						likes: elem.likes.count
					}
				});

				this.setState({
					jsonData: filteredData
				});
			
			});
	}

	loadingAnimation() {
		let listEls = document.querySelectorAll(`.${styles.feed_item}`),
				i = 0;

		function fadeInTimer(i) {
			if (i < listEls.length) {
				setTimeout(function () {
					listEls[i].classList.remove(`${styles.loading}`);
					i++;
					fadeInTimer(i);
				}, 250);
			}
		};
	
		fadeInTimer(i);

	}

	hideFeedListItems() {
		let listEls = document.querySelectorAll(`.${styles.feed_item}`),
			i = 0;

		for (i; i < listEls.length; ++i) {
			listEls[i].classList.add(`${styles.loading}`);
		}
	}

	handleSortByLikes() {
		let sortedByLikes = this.state.jsonData.sort( (a, b) => {
			return b.likes - a.likes;
		});

		this.hideFeedListItems();

		this.setState({
			jsonData: sortedByLikes,
			sortedByDate: false,
			sortedByLikes: true
		});
	}

	handleSortByDate() {
		let sortedByDate = this.state.jsonData.sort( (a, b) => {
			return b.timestamp - a.timestamp;
		});

		this.hideFeedListItems();

		this.setState({
			jsonData: sortedByDate,
			sortedByDate: true,
			sortedByLikes: false
		});
	}

	componentDidMount() {
		this.fetchInstagramFeed();
	}

	componentDidUpdate() {
		this.loadingAnimation();
	}

	render() {

		const { jsonData } = this.state;

		if (!jsonData) { return null; }

		return (

			<div className="App">
				<div className={styles.btn_wrapper}>
					<SortByLikes onSortByLikesClick={this.handleSortByLikes} sortedByLikes={this.state.sortedByLikes}/>
					<SortByDate onSortByDateClick={this.handleSortByDate} sortedByDate={this.state.sortedByDate}/>
				</div>
				<ul className={styles.feed_list}>
					{
						jsonData.map(elem => 
							<li key={elem.id} className={`${styles.feed_item} ${styles.loading}`}>
								<img src={elem.image_url} className={styles.feed_image}/>
							</li>
						)
					}
				</ul>
			</div>

		);
	
	}
}


export default App;