import React, { Component } from 'react';
import styles from './button.css';

class SortByLikes extends Component {

	constructor(props) {
		super(props);

		this.handleSortLikesClick = this.handleSortLikesClick.bind(this);
	}

	handleSortLikesClick() {
		if (this.props.sortedByLikes) {
			return false;
		}
		this.props.onSortByLikesClick();
	}

	render() {

		let active = this.props.sortedByLikes ? styles.active : '';

		return (
			<button className={`${styles.btn} ${active}`} onClick={this.handleSortLikesClick}>Most Liked</button>
		);
	
	}
}


export default SortByLikes;