import React, { Component } from 'react';
import styles from './button.css';

class SortByDate extends Component {

	constructor(props) {
		super(props);

		this.handleSortDateClick = this.handleSortDateClick.bind(this);
	}

	handleSortDateClick() {
		if (this.props.sortedByDate) {
			return false;
		}
		this.props.onSortByDateClick();
	}

	render() {

		let active = this.props.sortedByDate ? styles.active : '';

		return (
			<button className={`${styles.btn} ${active}`} onClick={this.handleSortDateClick}>Most Recent</button>
		);
	
	}
}


export default SortByDate;