const path = require('path'),
			webpack = require('webpack'),
			HtmlWebpackPlugin = require('html-webpack-plugin'),
			ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	
	entry: './src/',
	
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'build/sortable_insta_feed.js',
	},
	
	module:{
		loaders: [
			{
				test: /\.js/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['react','es2015']
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: [
						{ 
							loader: 'css-loader',
							options: {
								modules: true,
								sourceMap: true,
								minimize: true,
								importLoaders: 1,
								localIdentName: "[name]__[local]--[hash:base64:5]"
							}
						},
						'postcss-loader'
					]
				})
			}
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
      template: "./build/index.html"
    }),
		new ExtractTextPlugin("sortable_insta_feed.css")
	],

	devServer: {
		contentBase: path.join(__dirname, 'build'),
		compress: true,
		watchContentBase: true,
		inline: true,
		port: 9000
	}

};
